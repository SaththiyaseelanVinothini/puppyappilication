package Calculation;

public class PuppyDem {
	private String name;
	private int age;
	private String colour;
	public PuppyDem(String name, int age,String colour ) {

		this.name = name;
		this.age = age;
		this.colour=colour;
		
	}

	public void display() {

		System.out.println("I am " + name + " " + age + " years old."+ "I am "+colour+" colour.");
	}

}
